/**
 * Essas são as configurações DESSE projeto. Ele apenas faz uso do arquivo
 * shared.config.js e sobrescreve o que for interessante para ESSE projeto.
 */
const sharedConfig = require('./lib/shared.config')

module.exports = {
  branches: [...sharedConfig.branches],
  plugins: [
    sharedConfig.plugins.commitAnalyzer,
    sharedConfig.plugins.releaseNotes,
    sharedConfig.plugins.changelog,
    sharedConfig.plugins.gitlab,
    sharedConfig.plugins.npm,
    sharedConfig.plugins.git,
  ],
}
