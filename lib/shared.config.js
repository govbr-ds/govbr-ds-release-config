/**
 * Configurações compartilhadas do semantic-release.
 */

module.exports = {
  branches: [
    'main',
    '+([0-9])?(.{+([0-9]),x}).x', //1.x.x
    {
      name: 'next',
      prerelease: 'next',
    },
    // 'next-major',
    // {
    //   name: '+([0-9])?(.{+([0-9]),x}).x-rc',
    //   prerelease: 'rc',
    // },
    // {
    //   name: '+([0-9])?(.{+([0-9]),x}).x-beta',
    //   prerelease: 'beta',
    // },
    {
      name: '+([0-9])?(.{+([0-9]),x}).x-alpha',
      prerelease: 'alpha',
    },
    // {
    //   name: 'rc',
    //   prerelease: 'rc',
    // },
    // {
    //   name: 'beta',
    //   prerelease: 'beta',
    // },
    {
      name: 'alpha',
      prerelease: 'alpha',
    },
  ],
  plugins: {
    commitAnalyzer: [
      '@semantic-release/commit-analyzer',
      {
        preset: 'conventionalcommits',
        releaseRules: [
          // breaking precisa estar no topo: https://github.com/semantic-release/commit-analyzer#releaserules
          { breaking: true, release: 'major' },
          { type: '*!', release: 'major' },
          { type: 'build', release: false },
          { type: 'chore', release: false },
          { type: 'ci', release: false },
          { type: 'deprecated', release: false },
          { type: 'docs', release: 'patch' },
          { type: 'feat', release: 'minor' },
          { type: 'fix', release: 'patch' },
          { type: 'ops', release: false },
          { type: 'perf', release: 'patch' },
          { type: 'refactor', release: 'patch' },
          { type: 'removed', release: 'minor' },
          { type: 'revert', release: 'minor' },
          { type: 'lint', release: false },
          { type: 'test', release: false },
          { type: 'wip', release: false },
          { scope: 'no-release', release: false },
        ],
        parserOpts: {
          noteKeywords: ['BREAKING CHANGE', 'BREAKING CHANGES', 'BREAKING'],
        },
      },
    ],
    releaseNotes: [
      '@semantic-release/release-notes-generator',
      {
        preset: 'conventionalcommits',
        presetConfig: {
          header: '# CHANGELOG',
          types: [
            { type: 'deprecated', section: '👎 DEPRECIADO' },
            { type: 'feat', section: '✨ NOVIDADES' },
            { type: 'removed', section: '🚧 REMOVIDO' },
            { type: 'docs', section: '📚 DOCUMENTAÇÃO' },
            { type: 'fix', section: '🐛 CORREÇÕES' },
            { type: 'ops', section: '🔧 ATIVIDADES OPERACIONAIS' },
            { type: 'perf', section: '🚀 PERFORMANCE' },
            { type: 'refactor', section: '🔁 REFATORADO' },
            { type: 'revert', section: '⏪ REVERTIDO' },
            { type: 'build', hidden: true },
            { type: 'chore', hidden: true },
            { type: 'ci', hidden: true },
            { type: 'lint', hidden: true },
            { type: 'test', hidden: true },
            { type: 'wip', hidden: true },
          ],
        },
        parserOpts: {
          noteKeywords: ['BREAKING CHANGE', 'BREAKING CHANGES', 'BREAKING'],
        },
        writerOpts: {
          mainTemplate:
            '{{> header}}\n' +
            '\n' +
            '{{#if noteGroups}}\n' +
            '{{#each noteGroups}}\n' +
            '\n' +
            '### 💥 {{title}}\n' +
            '\n' +
            '{{#each notes}}\n' +
            '* {{#if commit.scope}}**{{commit.scope}}:** {{/if}}{{text}}\n' +
            '{{/each}}\n' +
            '{{/each}}\n' +
            '{{/if}}\n' +
            '{{#each commitGroups}}\n' +
            '\n' +
            '{{#if title}}\n' +
            '### {{title}}\n' +
            '\n' +
            '{{/if}}\n' +
            '{{#each commits}}\n' +
            '{{> commit root=@root}}\n' +
            '{{/each}}\n' +
            '\n' +
            '{{/each}}\n',
          commitsSort: ['scope', 'subject'],
          finalizeContext: (context) => {
            const date = new Date()
            context.date = date.toLocaleString('pt-BR', {
              timeZone: 'America/Sao_Paulo',
              dateStyle: 'short',
            })
            return context
          },
        },
      },
    ],
    changelog: [
      '@semantic-release/changelog',
      {
        changelogTitle: '# CHANGELOG',
      },
    ],
    gitlab: [
      '@semantic-release/gitlab',
      {
        successComment:
          ':tada: Issue/Merge Request incluído(a) na versão ${nextRelease.version} :tada:\n\nPara mais detalhes:\n- [GitLab release]($CI_PROJECT_URL/-/releases/${nextRelease.version})\n- [GitLab tag]($CI_PROJECT_URL/-/tags/${nextRelease.version})',
      },
    ],
    npm: ['@semantic-release/npm'],
    git: [
      '@semantic-release/git',
      {
        assets: ['package.json', 'CHANGELOG.md'],
        message:
          'chore(release): ${nextRelease.version} [skip ci] \n\n${nextRelease.notes} \n\nCommit gerado automaticamente durante o processo de release',
      },
    ],
  },
}
