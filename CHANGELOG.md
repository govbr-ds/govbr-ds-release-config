# CHANGELOG

## [2.3.1](https://gitlab.com/govbr-ds/configs/govbr-ds-release-config/compare/v2.3.0...v2.3.1) (19/10/2023)


### 🐛 CORREÇÕES

* **deps:** remove desnecessárias e coloca na área correta ([c4bbd0b](https://gitlab.com/govbr-ds/configs/govbr-ds-release-config/commit/c4bbd0b36016a539fd85b7006f7ee07f486e0cf3))

## [2.3.0](https://gitlab.com/govbr-ds/configs/govbr-ds-release-config/compare/v2.2.0...v2.3.0) (18/10/2023)


### ✨ NOVIDADES

* configurações agora estão no padrão do GovBR-DS ([21edf86](https://gitlab.com/govbr-ds/configs/govbr-ds-release-config/commit/21edf868c89d3979c5d7548a36c6bf6121127c5b))
* **dependências:** atualiza para as novas versões ([2d8793c](https://gitlab.com/govbr-ds/configs/govbr-ds-release-config/commit/2d8793c21c09e42c4e94d62c2580051a0cc90ada))

## [2.2.0](https://gitlab.com/govbr-ds/govbr-ds-release-config/compare/v2.1.2...v2.2.0) (04/08/2023)


### ✨ NOVIDADES

* atualiza padrão do changelog ([c662b2d](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/c662b2d465c2849bba88f6e54f5212850c5ead88))

## [2.1.2](https://gitlab.com/govbr-ds/govbr-ds-release-config/compare/v2.1.1...v2.1.2) (28/04/2023)


### 🪲 Correções

* altera os ícones de documentação conforme storybook e git ([8f66d6f](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/8f66d6f2a86cc69c50b767dbead493fbd65d8c06))

## [2.1.1](https://gitlab.com/govbr-ds/govbr-ds-release-config/compare/v2.1.0...v2.1.1) (13/04/2023)


### :bug: Correções

* **dependências:** remove @commitlint/cli e @commitlint/cz-commitlint das dependências de dev ([2e3fe7e](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/2e3fe7e925896981498132fde7f0753b973d3b78))
* **dependências:** remove a @govbr-ds/release-config ([86f0b99](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/86f0b99a157e97367daefbc8e187e8a82227ba54))

## [2.1.0](https://gitlab.com/govbr-ds/govbr-ds-release-config/compare/v2.0.0...v2.1.0) (10/04/2023)


### :sparkles: Novidades

* atualiza as depenndências e inclui exigência do node 18 ([6044cb0](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/6044cb0c9726478e2b8039d20028ac5e5aabf2f5))


### :memo: Documentação

* atualiza urls da wiki nas documentações ([a10ed64](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/a10ed64e9f893a0b8ab4fdb5bc7bfd597da7633e))


### :bug: Correções

* corrige o estágio de release ([cba87e8](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/cba87e85544a1d17a7ba0c8f7d1f9c0828752352))

## [2.0.0](https://gitlab.com/govbr-ds/govbr-ds-release-config/compare/v1.4.0...v2.0.0) (01/03/2023)


### ⚠ BREAKING CHANGES

* o semantic release agora exige o node 18 e é esm-only

### :sparkles: Novidades

* atualiza dependencias e pipeline de release ([3d468d5](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/3d468d550e0eff16c46019dc2282326dc98886b9))


### :memo: Documentação

* atualiza email no readme ([2b4311e](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/2b4311eed4caed5c3a30da642fe026fc9079f655))

## [1.4.0](https://gitlab.com/govbr-ds/govbr-ds-release-config/compare/v1.3.0...v1.4.0) (28/12/2022)


### :sparkles: Novidades

* nova configuração para os canais de distribuição ([ec12945](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/ec1294583a5a5155559961664adef14c18ead59b))

## [1.3.0](https://gitlab.com/govbr-ds/govbr-ds-release-config/compare/v1.2.1...v1.3.0) (22/12/2022)


### :sparkles: Novidades

* ajusta os canais de distribuição ([cad2b69](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/cad2b699799aa95c5208638d87f9d0a7857d0252))

## [1.2.1](https://gitlab.com/govbr-ds/govbr-ds-release-config/compare/v1.2.0...v1.2.1) (16/12/2022)


### :bug: Correções

* altera footer do commit de release ([edbb291](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/edbb291c4e5114d94aaec0fb0c45e19d254bdf25))

## [1.2.0](https://gitlab.com/govbr-ds/govbr-ds-release-config/compare/v1.1.3...v1.2.0) (13/12/2022)


### :sparkles: Novidades

* novos padrão para canais de distribuição ([2e49177](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/2e491771870e21aa82001665483bb41c3287121e))

## [1.1.3](https://gitlab.com/govbr-ds/govbr-ds-release-config/compare/v1.1.2...v1.1.3) (09/12/2022)


### :bug: Correções

* variável ci_project_url não encontrada ([846a3e9](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/846a3e9e48ee747202501b3c9695c5af6d189781))

## [1.1.2](https://gitlab.com/govbr-ds/govbr-ds-release-config/compare/v1.1.1...v1.1.2) (09/12/2022)


### :memo: Documentação

* atualiza o readme ([f8e6a4c](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/f8e6a4cc28da3772aa4091b4df32dd0b3c8c9734))


### :bug: Correções

* **dependências:** dependências foram atualizadas para novas versões ([ca6fa4a](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/ca6fa4abf659fc8c30dae62846a40712400924e1))

## [1.1.1](https://gitlab.com/govbr-ds/govbr-ds-release-config/compare/v1.1.0...v1.1.1) (17/10/2022)


### :bug: Correções

* corrige as dependências de dev ([e0ebb09](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/e0ebb0900abfe34f544336228dc8ab9c46293e7e))

## [1.1.0](https://gitlab.com/govbr-ds/govbr-ds-release-config/compare/v1.0.1...v1.1.0) (14/10/2022)


### :sparkles: Novidades

* altera a exportação para facilitar a customização ([bfb5558](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/bfb555825f0da523bbfed98416976c9b5ca1ded3))


### :bug: Correções

* altera padrões de commit conforme wiki ([635fa10](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/635fa1077c60c930d1366883296e69e02b2b71f2))
* ícone para novas funcionalidades ([0a91c78](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/0a91c78044ae249ca36b4fb62a6cd22204bb62a7))
* padrão de data dos changelogs para padrão brasileiro ([bace309](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/bace309e4507514439b86ae0d39cd2f3842a6eb5))

## [1.1.0-alpha.4](https://gitlab.com/govbr-ds/govbr-ds-release-config/compare/v1.1.0-alpha.3...v1.1.0-alpha.4) (14/10/2022)


### :bug: Correções

* padrão de data dos changelogs para padrão brasileiro ([bace309](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/bace309e4507514439b86ae0d39cd2f3842a6eb5))

## [1.1.0-alpha.3](https://gitlab.com/govbr-ds/govbr-ds-release-config/compare/v1.1.0-alpha.2...v1.1.0-alpha.3) (14/10/2022)


### :bug: Correções

* altera padrões de commit conforme wiki ([635fa10](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/635fa1077c60c930d1366883296e69e02b2b71f2))

## [1.1.0-alpha.2](https://gitlab.com/govbr-ds/govbr-ds-release-config/compare/v1.1.0-alpha.1...v1.1.0-alpha.2) (14/10/2022)


### :bug: Correções

* ícone para novas funcionalidades ([0a91c78](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/0a91c78044ae249ca36b4fb62a6cd22204bb62a7))

## [1.1.0-alpha.1](https://gitlab.com/govbr-ds/govbr-ds-release-config/compare/v1.0.1...v1.1.0-alpha.1) (14/10/2022)


### :new: Novidades

* altera a exportação para facilitar a customização ([bfb5558](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/bfb555825f0da523bbfed98416976c9b5ca1ded3))

## [1.0.1](https://gitlab.com/govbr-ds/govbr-ds-release-config/compare/v1.0.0...v1.0.1) (13/10/2022)


### :memo: Documentação

* inclui o npm na lista de plugins ([d6fdc84](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/d6fdc84f54b30afcaba06133b6cf4f6a63a8e870))


### :bug: Correções

* versionamento de breaking changes e formato das datas ([eede37d](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/eede37d14bf5ed6ce3b2275b6332700d9c22b60c))

## 1.0.0 (2022-10-10)


### Features

* commit inicial ([dc17069](https://gitlab.com/govbr-ds/govbr-ds-release-config/commit/dc170699f19af90c13613169f6a375133ca261c8))
