# GOVBR-DS - Commit Config

## Objetivo

Compartilhar os padrões de commit entre os projetos do [GOVBR-DS](https://gov.br/ds 'GOVBR-DS').

## Como instalar nossa biblioteca em seu projeto?

```bash
npm install --save-dev @govbr-ds/release-config

ou

pnpm add -D @govbr-ds/release-config
```

## Plugins

Nessa configuração nós usamos os seguintes plugins:

- [`@semantic-release/commit-analyzer`](https://github.com/semantic-release/commit-analyzer)
- [`@semantic-release/release-notes-generator`](https://github.com/semantic-release/release-notes-generator)
- [`@semantic-release/npm`](https://github.com/semantic-release/npm)
- [`@semantic-release/changelog`](https://github.com/semantic-release/changelog)
- [`@semantic-release/gitlab`](https://github.com/semantic-release/gitlab)

## Como usar?

A configuração compartilhada pode ser configurada conforme o [arquivo de configuração do semantic-release](https://github.com/semantic-release/semantic-release/blob/master/docs/usage/configuration.md#configuration):

### Exemplo

```javascript
const sharedConfig = require('@govbr-ds/release-config')

module.exports = {
  branches: [...sharedConfig.branches],
  plugins: [
    sharedConfig.plugins.commitAnalyzer,
    sharedConfig.plugins.releaseNotes,
    // Configuração sobrescrita
    [
      '@semantic-release/changelog',
      {
        changelogTitle: '# CHANGELOG',
      },
    ],
    sharedConfig.plugins.gitlab,
    sharedConfig.plugins.npm,
    sharedConfig.plugins.git,
    // Plugin não configurado no padrão (consulte a documentação do plugin para mais detalhes)
    [
      'semantic-release-discord',
      {
        onSuccessTemplate: {
          username: 'Username',
          content: 'A versão v$npm_package_version do projeto LOREM IPSUM já está disponível!',
        },
      },
    ],
  ],
}
```

### Configuração

Consulta a documentação de cada [plugin](#plugins) para entender o passo-a-passo para extender a configuração padrão.

## Como contribuir?

Antes de abrir um Merge Request tenha em mente algumas informações:

- Esse é um projeto opensource e contribuições são bem-vindas.
- Para facilitar a aprovação da sua contribuição, escolha um título curto, simples e explicativo para o MR, e siga os padrões da nossa [wiki](https:/gov.br/ds/wiki/ 'Wiki').
- Quer contribuir com o projeto? Confira o nosso guia [como contribuir](./CONTRIBUTING.md 'Como contribuir?').

## Reportar bugs/necessidades

Você pode usar as [issues](https://gitlab.com/govbr-ds/govbr-ds-release-config/-/issues/new) para nos informar os problemas que tem enfrentado ao usar nossa biblioteca ou mesmo o que gostaria que fizesse parte do projeto. Por favor use o modelo que mais se encaixa na sua necessidade e preencha com o máximo de detalhes possível.

Nos comprometemos a responder a todas as issues

## Precisa de ajuda?

> Por favor **não** crie issues para fazer perguntas...

Use nossos canais abaixo para obter tirar suas dúvidas:

- Site do GOVBR-DS [http://gov.br/ds](http://gov.br/ds)

- Pelo nosso email <govbr-ds@serpro.gov.br>

- Usando nosso canal no discord [https://discord.gg/U5GwPfqhUP](https://discord.gg/U5GwPfqhUP)

## Padrão de commits

Para mais informações sobre o padrão de commits consulte [a nossa Wiki](https:/gov.br/ds/wiki/git-gitlab/guias/commit/ 'Padrão de commit').

## Licença

Nesse projeto usamos a licença MIT.
